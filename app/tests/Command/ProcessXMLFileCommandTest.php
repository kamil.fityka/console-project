<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Feed;
use App\Exception\DirNotExistException;
use App\Exception\FileNameExtensionException;
use App\Exception\NoReaderExistException;
use App\Service\Database\DatabaseInterface;
use App\Service\Database\MysqlEntityManager;
use App\Service\EntityManagerFactory;
use App\Service\Reader\LocalFileReader;
use App\Service\Reader\RemoteFileReader;
use App\Service\ReaderFactory;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

final class ProcessXMLFileCommandTest extends TestCase
{
    private ?EntityRepository $repository;
    private ?DatabaseInterface $entityManager;
    private ?CommandTester $commandTester;

    protected function setUp(): void
    {
        $application = new Application();

        $this->entityManager = new MysqlEntityManager();
        $this->entityManager->getEntityManager()->beginTransaction();
        $this->repository = $this->entityManager->getEntityManager()->getRepository(Feed::class);

        $command = new ProcessXMLFileCommand(
            new ReaderFactory([
                new LocalFileReader(),
                new RemoteFileReader()
            ]),
            new EntityManagerFactory([
                $this->entityManager
            ])
        );
        $application->add($command);

        $this->commandTester = new CommandTester($command);
    }

    /**
     * @dataProvider getCorrectExecuteData
     */
    public function testCorrectExecut(array $data)
    {
        $status = $this->commandTester->execute([
            'type' => $data[0],
            'dir' => $data[1],
            'fileName' => $data[2],
        ]);

        $this->assertEquals($status, Command::SUCCESS);
        $this->assertEquals(12, count($this->repository->findAll()));
    }

    /**
     * @dataProvider getExceptionExecuteData
     */
    public function testExceptionExecut(array $data)
    {
        $status = $this->commandTester->execute([
            'type' => $data[0],
            'dir' => $data[1],
            'fileName' => $data[2],
        ]);

        $this->assertEquals($status, Command::FAILURE);
        $this->assertEquals(0, count($this->repository->findAll()));
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->getEntityManager()->getConnection()->rollback();
        $this->entityManager = null;
        $this->commandTester = null;
    }

    private function getCorrectExecuteData(): array
    {
        return [
          [[
              LocalFileReader::TYPE,
              'file:///var/www/app/tests/data/',
              'coffee_feed_trimmed.xml'
          ]],
          [[
              RemoteFileReader::TYPE,
              'ftp://pupDev:pupDev2018@transport.productsup.io/',
              'coffee_feed_trimmed.xml'
          ]],
        ];
    }

    private function getExceptionExecuteData(): array
    {
        return [
            [[
                'test',
                'file:///var/www/app/tests/data/',
                'coffee_feed_trimmed.xml',
                NoReaderExistException::class
            ]],
            [[
                LocalFileReader::TYPE,
                'file:///var/www/app/xxxxxxx/',
                'coffee_feed_trimmed.xml',
                DirNotExistException::class
            ]],
            [[
                LocalFileReader::TYPE,
                'file:///var/www/app/tests/data/',
                'coffee.txt',
                FileNameExtensionException::class
            ]],
        ];
    }
}
