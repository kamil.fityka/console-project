<?php

declare(strict_types=1);

namespace App\Test\Validator;

use App\Exception\FileNameExtensionException;
use App\Validator\FileNameExtensionValidator;
use PHPUnit\Framework\TestCase;

class FileNameExtensionValidatorTest extends TestCase
{
    public function testFileExtensionValid()
    {
        $validator = new FileNameExtensionValidator('example.xml');

        $this->assertTrue($validator->valid());
    }

    public function testFileExtensionNotValid()
    {
        $this->expectException(FileNameExtensionException::class);

        $validator = new FileNameExtensionValidator('example.txt');
        $validator->valid();
    }
}
