<?php

declare(strict_types=1);

use App\Exception\NoReaderExistException;
use App\Service\ReaderFactory;
use PHPUnit\Framework\TestCase;
use App\Service\Reader\LocalFileReader;
use App\Service\Reader\RemoteFileReader;

class ReaderFactoryTest extends TestCase
{
    private ReaderFactory $factory;

    protected function setUp(): void
    {
        $this->factory = new ReaderFactory([
            new LocalFileReader(),
            new RemoteFileReader()
        ]);
    }

    /**
     * @dataProvider getReadersDataProvider
     */
    public function testCorrectReaderTest(array $readerParams)
    {
        $reader = $this->factory->create($readerParams[0], $readerParams[1], $readerParams[2]);

        $this->assertEquals($reader::class, $readerParams[3]);
    }

    public function testThrowExceptionTest()
    {
        $this->expectException(NoReaderExistException::class);
        $this->factory->create('test', 'path/', 'test.xml');
    }

    public function getReadersDataProvider()
    {
        return [
            [[
                LocalFileReader::TYPE,
                'path/to/dir',
                'text.xml',
                LocalFileReader::class
            ]],
            [[
                RemoteFileReader::TYPE,
                'path/to/dir',
                'text.xml',
                RemoteFileReader::class
            ]],
        ];
    }
}
