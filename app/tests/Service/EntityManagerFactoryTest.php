<?php

declare(strict_types=1);

use App\Exception\NoReaderExistException;
use App\Service\EntityManagerFactory;
use PHPUnit\Framework\TestCase;
use App\Service\Database\MysqlEntityManager;

class EntityManagerFactoryTest extends TestCase
{
    private EntityManagerFactory $factory;

    protected function setUp(): void
    {
        $this->factory = new EntityManagerFactory([
            new MysqlEntityManager()
        ]);
    }

    /**
     * @dataProvider getDatabaseDataProvider
     */
    public function testCorrectEntityManagerTest(array $readerParams)
    {
        $reader = $this->factory->create($readerParams[0]);

        $this->assertEquals($reader::class, $readerParams[1]);
    }

    public function testThrowExceptionTest()
    {
        $this->expectException(NoReaderExistException::class);
        $this->factory->create('test');
    }

    public function getDatabaseDataProvider()
    {
        return [
            [[
                MysqlEntityManager::TYPE,
                MysqlEntityManager::class
            ]],
        ];
    }
}
