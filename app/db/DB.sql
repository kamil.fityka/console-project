DROP TABLE IF EXISTS `feeds`;

CREATE TABLE `feeds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) unsigned NOT NULL,
  `category_name` varchar(180) NOT NULL,
  `sku` bigint(20) unsigned NOT NULL,
  `name` varchar(180) NOT NULL,
  `description` text,
  `shortdesc` text NOT NULL,
  `price` varchar(180) NOT NULL,
  `link` varchar(180) NOT NULL,
  `image` varchar(180) NOT NULL,
  `brand` varchar(180) NOT NULL,
  `rating` int(10) unsigned NOT NULL,
  `caffeine_type` varchar(180) DEFAULT NULL,
  `count` int(10) unsigned DEFAULT NULL,
  `flavored` varchar(180) DEFAULT NULL,
  `seasonal` varchar(180) DEFAULT NULL,
  `instock` varchar(180) NOT NULL,
  `facebook` int(10) unsigned NOT NULL,
  `is_k_cup` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

LOCK TABLES `feeds` WRITE;
UNLOCK TABLES;