<?php

declare(strict_types=1);

namespace App\Validator;

use App\Exception\FileNameExtensionException;

final class FileNameExtensionValidator
{
    public const EXTENSIONS = ['xml'];

    public function __construct(private string $value)
    {
    }

    public function valid(): bool
    {
        if (!preg_match('/\.(' . implode('|', self::EXTENSIONS) . ')$/', $this->value)) {
            throw new FileNameExtensionException($this->value, self::EXTENSIONS);
        }

        return true;
    }
}
