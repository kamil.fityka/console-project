<?php

namespace App\Exception;

final class NoReaderExistException extends \Exception
{
    public function __construct(string $readerName, int $code = 0, ?\Throwable $previous = null)
    {
        $message = sprintf('Reader "%s" not exist', $readerName);

        parent::__construct($message, $code, $previous);
    }
}
