<?php

namespace App\Exception;

final class FileNotExistException extends \Exception
{
    public function __construct(string $dir, int $code = 0, ?\Throwable $previous = null)
    {
        $message = sprintf('The file "%s" not exist', $dir);

        parent::__construct($message, $code, $previous);
    }
}
