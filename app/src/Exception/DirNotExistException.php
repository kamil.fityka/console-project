<?php

namespace App\Exception;

final class DirNotExistException extends \Exception
{
    public function __construct(string $dir, int $code = 0, ?\Throwable $previous = null)
    {
        $message = sprintf('The "%s" not exist', $dir);

        parent::__construct($message, $code, $previous);
    }
}
