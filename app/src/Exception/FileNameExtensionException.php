<?php

namespace App\Exception;

final class FileNameExtensionException extends \Exception
{
    public function __construct(string $value, array $extensions, int $code = 0, ?\Throwable $previous = null)
    {
        $message = sprintf(
            "File extension '%s' is incorrect. Allowed: '%s'",
            $value,
            implode(', ', $extensions)
        );

        parent::__construct($message, $code, $previous);
    }
}
