<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="feeds")
 */
#[ORM\Entity]
#[ORM\Table(name: 'feeds')]
class Feed
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    #[ORM\Id]
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\GeneratedValue]
    private int $id;
    /** @ORM\Column(type="integer", name="entity_id") */
    #[ORM\Column(name: 'entity_id', type: Types::INTEGER)]
    private int $entityId;
    /** @ORM\Column(type="string", name="category_name") */
    #[ORM\Column(name: 'category_name', type: Types::STRING)]
    private string $categoryName;
    /** @ORM\Column(type="integer") */
    #[ORM\Column(type: Types::INTEGER)]
    private int $sku;
    /** @ORM\Column(type="string") */
    #[ORM\Column(type: Types::STRING)]
    private string $name;
    /** @ORM\Column(type="string", nullable=true) */
    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $description;
    /** @ORM\Column(type="string") */
    #[ORM\Column(type: Types::STRING)]
    private string $shortdesc;
    /** @ORM\Column(type="float") */
    #[ORM\Column(type: Types::FLOAT)]
    private float $price;
    /** @ORM\Column(type="string") */
    #[ORM\Column(type: Types::STRING)]
    private string $link;
    /** @ORM\Column(type="string") */
    #[ORM\Column(type: Types::STRING)]
    private string $image;
    /** @ORM\Column(type="string") */
    #[ORM\Column(type: Types::STRING)]
    private string $brand;
    /** @ORM\Column(type="integer") */
    #[ORM\Column(type: Types::INTEGER)]
    private int $rating;
    /** @ORM\Column(type="string", nullable=true, name="caffeine_type") */
    #[ORM\Column(name: 'caffeine_type', type: Types::STRING, nullable: true)]
    private ?string $caffeineType;
    /** @ORM\Column(type="integer", nullable=true) */
    #[ORM\Column(type: Types::INTEGER, nullable: true)]
    private ?int $count;
    /** @ORM\Column(type="string", nullable=true) */
    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $flavored;
    /** @ORM\Column(type="string", nullable=true) */
    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $seasonal;
    /** @ORM\Column(type="string") */
    #[ORM\Column(type: Types::STRING)]
    private string $instock;
    /** @ORM\Column(type="integer") */
    #[ORM\Column(type: Types::INTEGER)]
    private int $facebook;
    /** @ORM\Column(type="integer", name="is_k_cup") */
    #[ORM\Column(name: 'is_k_cup', type: Types::INTEGER)]
    private int $isKCup;

    public function getId(): int
    {
        return $this->id;
    }

    public function getEntityId(): int
    {
        return $this->entityId;
    }

    public function setEntityId(int $entityId): void
    {
        $this->entityId = $entityId;
    }

    public function getCategoryName(): string
    {
        return $this->categoryName;
    }

    public function setCategoryName(string $categoryName): void
    {
        $this->categoryName = $categoryName;
    }

    public function getSku(): int
    {
        return $this->sku;
    }

    public function setSku(int $sku): void
    {
        $this->sku = $sku;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getShortdesc(): string
    {
        return $this->shortdesc;
    }

    public function setShortdesc(string $shortdesc): void
    {
        $this->shortdesc = $shortdesc;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    public function getBrand(): string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    public function getRating(): int
    {
        return $this->rating;
    }

    public function setRating(int $rating): void
    {
        $this->rating = $rating;
    }

    public function getCaffeineType(): ?string
    {
        return $this->caffeineType;
    }

    public function setCaffeineType(?string $caffeineType): void
    {
        $this->caffeineType = $caffeineType;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(?int $count): void
    {
        $this->count = $count;
    }

    public function getFlavored(): ?string
    {
        return $this->flavored;
    }

    public function setFlavored(?string $flavored): void
    {
        $this->flavored = $flavored;
    }

    public function getSeasonal(): ?string
    {
        return $this->seasonal;
    }

    public function setSeasonal(?string $seasonal): void
    {
        $this->seasonal = $seasonal;
    }

    public function getInstock(): string
    {
        return $this->instock;
    }

    public function setInstock(string $instock): void
    {
        $this->instock = $instock;
    }

    public function getFacebook(): int
    {
        return $this->facebook;
    }

    public function setFacebook(int $facebook): void
    {
        $this->facebook = $facebook;
    }

    public function getIsKCup(): int
    {
        return $this->isKCup;
    }

    public function setIsKCup(int $isKCup): void
    {
        $this->isKCup = $isKCup;
    }
}
