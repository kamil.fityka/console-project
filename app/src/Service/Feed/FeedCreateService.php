<?php

declare(strict_types=1);

namespace App\Service\Feed;

use App\Entity\Feed;
use App\Model\Feed as FeedModel;
use App\Model\FeedCollection;

class FeedCreateService
{
    private function create(FeedModel $feedModel): Feed
    {
        $feed = new Feed();
        $feed->setEntityId($feedModel->getId());
        $feed->setCategoryName($feedModel->getCategoryName());
        $feed->setSku($feedModel->getSku());
        $feed->setName($feedModel->getName());
        $feed->setDescription($feedModel->getDescription());
        $feed->setShortdesc($feedModel->getShortdesc());
        $feed->setPrice($feedModel->getPrice());
        $feed->setLink($feedModel->getLink());
        $feed->setImage($feedModel->getImage());
        $feed->setBrand($feedModel->getBrand());
        $feed->setRating($feedModel->getRating());
        $feed->setCaffeineType($feedModel->getCaffeineType());
        $feed->setCount($feedModel->getCount());
        $feed->setFlavored($feedModel->getFlavored());
        $feed->setSeasonal($feedModel->getSeasonal());
        $feed->setInstock($feedModel->getInstock());
        $feed->setFacebook($feedModel->getFacebook());
        $feed->setIsKCup($feedModel->getIsKCup());

        return $feed;
    }

    public function createFeeds(FeedCollection $collection): array
    {
        $feeds = [];
        foreach ($collection->getFeeds() as $feedModel) {
            $feeds[] = $this->create($feedModel);
        }

        return $feeds;
    }
}
