<?php

declare(strict_types=1);

namespace App\Service;

use App\Service\DataBase\DataBaseInterface;
use App\Exception\NoReaderExistException;

final class EntityManagerFactory
{
    public function __construct(private iterable $databaseServices)
    {
    }

    public function create(string $name): DataBaseInterface
    {
        /** @var DataBaseInterface $database */
        foreach ($this->databaseServices as $service) {
            if ($service->isSupport($name)) {
                return $service;
            }
        }

        throw new NoReaderExistException($name);
    }
}
