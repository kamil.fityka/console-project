<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\NoReaderExistException;
use App\Service\Reader\XmlFileReaderInterface;

final class ReaderFactory
{
    public function __construct(private iterable $readers)
    {
    }

    public function create(string $name, string $dir, string $fileName): XmlFileReaderInterface
    {
        /** @var XmlFileReaderInterface $reader */
        foreach ($this->readers as $reader) {
            if ($reader->isSupport($name)) {
                return $reader
                    ->setDir($dir)
                    ->setFileName($fileName);
            }
        }

        throw new NoReaderExistException($name);
    }
}
