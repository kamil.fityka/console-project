<?php

declare(strict_types=1);

namespace App\Service\Reader;

final class RemoteFileReader extends CommonFileReader implements XmlFileReaderInterface
{
    public const TYPE = 'remote';

    public function getType(): string
    {
        return self::TYPE;
    }
}
