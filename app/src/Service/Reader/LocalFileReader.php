<?php

declare(strict_types=1);

namespace App\Service\Reader;

final class LocalFileReader extends CommonFileReader implements XmlFileReaderInterface
{
    public const TYPE = 'local';

    public function getType(): string
    {
        return self::TYPE;
    }
}
