<?php

declare(strict_types=1);

namespace App\Service\Reader;

use App\Model\FeedCollection;

interface XmlFileReaderInterface
{
    public function isSupport(string $name): bool;
    public function read(): FeedCollection;
    public function getType(): string;
}
