<?php

declare(strict_types=1);

namespace App\Service\Reader;

use App\Model\FeedCollection;
use App\Exception\DirNotExistException;
use App\Exception\FileNotExistException;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Finder\Finder;

abstract class CommonFileReader
{
    private Finder $finder;

    public function __construct()
    {
        $this->finder = new Finder();
    }

    #[Pure]
    public function isSupport(string $name): bool
    {
        return $name === $this->getType();
    }

    public function getDir(): string
    {
        return $this->dir;
    }

    public function setDir(string $dir): self
    {
        $this->dir = $dir;

        return $this;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    #[Pure]
    private function getPath(): string
    {
        return sprintf(
            '%s%s',
            $this->getDir(),
            $this->getFileName()
        );
    }

    private function isDirExist(): bool
    {
        if (!$this->finder->in($this->getDir())->hasResults()) {
            throw new DirNotExistException($this->getDir());
        }

        return true;
    }

    private function isPathExist(Finder $finder): void
    {
        if (!$finder->hasResults()) {
            throw new FileNotExistException($this->getPath());
        }
    }

    private function finder(): Finder
    {
        return $this->finder->name($this->getFileName())->in($this->getDir());
    }

    public function read(): FeedCollection
    {
        $this->isDirExist();

        $finder = $this->finder();

        $this->isPathExist($finder);

        $feedFile = $finder->files()->getIterator()->current()->getContents();
        $feeds = simplexml_load_string($feedFile);

        return new FeedCollection($feeds);
    }
}
