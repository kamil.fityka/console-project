<?php

declare(strict_types=1);

namespace App\Service\Database;

use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMSetup;
use JetBrains\PhpStorm\ArrayShape;

final class MysqlEntityManager extends BaseEntityManager implements DatabaseInterface
{
    public const TYPE = 'mysql';

    public function __construct()
    {
        parent::__construct(EntityManager::create(
            $this->getDatabaseParams(),
            $this->getORMSetup()
        ));
    }

    public function getORMSetup(): Configuration
    {
        return ORMSetup::createAnnotationMetadataConfiguration(
            [
                self::ENTITY_PATH]
        );
    }

    #[ArrayShape([
        'driver' => "string",
        'user' => "string",
        'password' => "string",
        'dbname' => "string",
        'host' => "string",
        'port' => "int"
    ])]
    public function getDatabaseParams(): array
    {
        return [
            'driver' => $_ENV['APP_DB_DRIVER'],
            'user' => $_ENV['APP_DB_USER'],
            'password' => $_ENV['APP_DB_PASSWORD'],
            'dbname' => $_ENV['APP_DB_NAME'],
            'host' => $_ENV['APP_DB_HOST'],
            'port' => $_ENV['APP_DB_POST']
        ];
    }

    public function isSupport(string $name): bool
    {
        return self::TYPE === $name;
    }

    public function saveFeeds(array $entityFeeds): void
    {
        $this->em->getConnection()->beginTransaction();
        try {
            foreach ($entityFeeds as $feed) {
                $this->em->persist($feed);
            }
            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            throw $e;
        }
    }
}
