<?php

declare(strict_types=1);

namespace App\Service\Database;

use Doctrine\ORM\EntityManagerInterface;

interface DatabaseInterface
{
    public function isSupport(string $name): bool;
    public function getEntityManager(): EntityManagerInterface;
    public function getDatabaseParams(): array;
    public function saveFeeds(array $entityFeeds): void;
}
