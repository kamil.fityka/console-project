<?php

declare(strict_types=1);

namespace App\Service\Database;

use Doctrine\ORM\EntityManagerInterface;

abstract class BaseEntityManager
{
    public const ENTITY_PATH = 'src/Entity';

    public function __construct(protected EntityManagerInterface $em)
    {
    }

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }
}
