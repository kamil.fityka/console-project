<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\Database\MysqlEntityManager;
use App\Service\EntityManagerFactory;
use App\Service\Feed\FeedCreateService;
use App\Service\ReaderFactory;
use App\Validator\FileNameExtensionValidator;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class ProcessXMLFileCommand extends Command
{
    private LoggerInterface $logger;

    public function __construct(
        private ReaderFactory $readerFactory,
        private EntityManagerFactory $entityManagerFactory,
        private ?string $name = null,
    ) {
        $this->logger = new Logger('console');
        $this->logger->pushHandler(new StreamHandler('var/log/console.log'));

        parent::__construct($this->name);
    }

    protected static $defaultName = 'app:process-xml-file';

    protected function configure(): void
    {
        $this
            ->setHelp('This command process xml file')
            ->addArgument(
                'type',
                InputArgument::REQUIRED,
                'XML file is local or remote?'
            )
            ->addArgument(
                'dir',
                InputArgument::REQUIRED,
                'What is path to file (e.x. file:///var/www/app/data/ or ftp://user:password@example.com/pub/)?'
            )
            ->addArgument(
                'fileName',
                InputArgument::REQUIRED,
                'Please enter filename (e.x. data.xml): '
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $fileType = $input->getArgument('type');
        $dir = $input->getArgument('dir');
        $fileName = $input->getArgument('fileName');

        try {
            $this->logger->debug('File name validation');

            (new FileNameExtensionValidator($fileName))->valid();

            $this->logger->debug('File name valid');
            $this->logger->debug('Create reader service');

            $reader = $this->readerFactory->create($fileType, $dir, $fileName);

            $this->logger->debug(sprintf('Reader service created: %s', $reader::class));
            $this->logger->debug('Create feed collection');

            $collection = $reader->read();

            $this->logger->debug('Feed collection created');
            $this->logger->debug('Create entity manager');

            $entityManager = $this->entityManagerFactory->create(MysqlEntityManager::TYPE);

            $this->logger->debug(sprintf('Entity manager created: %s', $entityManager::class));
            $this->logger->debug('Start saving feeds');

            $feeds = (new FeedCreateService())->createFeeds($collection);
            $entityManager->saveFeeds($feeds);

            $this->logger->debug('Feeds saved');
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
