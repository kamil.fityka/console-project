<?php

declare(strict_types=1);

namespace App\Model;

final class Feed
{
    private int $id;
    private string $categoryName;
    private int $sku;
    private string $name;
    private ?string $description;
    private string $shortdesc;
    private float $price;
    private string $link;
    private string $image;
    private string $brand;
    private int $rating;
    private ?string $caffeineType;
    private ?int $count;
    private ?string $flavored;
    private ?string $seasonal;
    private string $instock;
    private int $facebook;
    private int $isKCup;

    public function __construct(\SimpleXMLElement $feed)
    {
        $this->id = (int) $feed->entity_id->__toString();
        $this->categoryName = $feed->CategoryName->__toString();
        $this->sku = (int) $feed->sku->__toString();
        $this->name = $feed->name->__toString();
        $this->description = $feed->description->__toString();
        $this->shortdesc = $feed->shortdesc->__toString();
        $this->price = (float) $feed->price->__toString();
        $this->link = $feed->link->__toString();
        $this->image = $feed->image->__toString();
        $this->brand = $feed->Brand->__toString();
        $this->rating = (int) $feed->Rating->__toString();
        $this->caffeineType = $feed->CaffeineType->__toString();
        $this->count = $feed->Count ? (int) $feed->Count->__toString() : null;
        $this->flavored = $feed->Flavored->__toString();
        $this->seasonal = $feed->Seasonal->__toString();
        $this->instock = $feed->Instock->__toString();
        $this->facebook = (int) $feed->Facebook->__toString();
        $this->isKCup = (int) $feed->IsKCup->__toString();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCategoryName(): string
    {
        return $this->categoryName;
    }

    public function getSku(): int
    {
        return $this->sku;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getShortdesc(): string
    {
        return $this->shortdesc;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getBrand(): string
    {
        return $this->brand;
    }

    public function getRating(): int
    {
        return $this->rating;
    }

    public function getCaffeineType(): ?string
    {
        return $this->caffeineType;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function getFlavored(): ?string
    {
        return $this->flavored;
    }

    public function getSeasonal(): ?string
    {
        return $this->seasonal;
    }

    public function getInstock(): string
    {
        return $this->instock;
    }

    public function getFacebook(): int
    {
        return $this->facebook;
    }

    public function getIsKCup(): int
    {
        return $this->isKCup;
    }
}
