<?php

declare(strict_types=1);

namespace App\Model;

final class FeedCollection
{
    private array $feeds = [];

    public function __construct($feeds)
    {
        if ($feeds instanceof \SimpleXMLElement) {
            foreach ($feeds as $feed) {
                $this->feeds[] = new Feed($feed);
            }
        }
    }

    public function getFeeds(): array
    {
        return $this->feeds;
    }
}
